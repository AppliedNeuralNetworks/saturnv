#   Saturn V
### A powerful application launcher.
---

## What is Saturn V?

Saturn V is a Python package that launches the Python application it is a part of. It uses some simple rules (and some assumptions) to determine your project's directory structure and configuration location. Along the way, it adds library paths, picks up configuration information, and determines the location of other data. Because this information is determined at run-time, Saturn V works equally well for self-contained applications as well as system-installed applications.

If you just need to fire off your program with the right library paths, you can just create a simple directory structure and a bit of code, drop in Saturn V, create a single symlink, and blast off! Meanwhile, if you need configuration information and more sophisticated path information at runtime, add this one-liner to your app:

```python
import saturnv

class Application():
    def __init__(self):
        self.conf = saturnv.AppConf()
```

## How to Use Saturn V

### 1. Decide on a Name for Your Executable

Saturn V begins to learn about your project's structure by examining the `basename()` of the executable. This example will refer to a project named "Example Project", for which the user will type `example` to launch the application.

### 2. Create a Directory Structure for Your Project

Saturn V learns more about your project by examining the directory structure. By following just a few conventions, we can give Saturn V everything it needs to launch your application.

Many projects start out in a new directory of their own, with subdirectories carved out and waiting to be filled with all the parts of the project. Saturn V detects this, searching upward for specifically named directories which demarcate the root of your project. First, create a top-level directory (the name is not significant). Inside this directory, create these subdirectories:

 * `bin/` will contain the symlink which will execute your application
 * `conf/` will contain the main configuration for the application
 * `lib/` will contain your application's packages

### 3. Create Your Application Core Package

In `lib/`, create a directory called `example` with the required `__init__.py`. This package must either have an `Application` class or an `application` namespace in which there is an `Application` class.  This class must have a `run()` method. (Note that if the filename of you your executable contains hyphens, the package that will be imported will substitute underscores for hyphens, since the latter is not allowed in Python. This only applies to the package import, so configuration files and other references to your program at run time will preserve the hyphens.)

### 4. Place Saturn V in `lib/`

It is sufficient to clone Saturn V into your lib directory:

`https://gitlab.com/AppliedNeuralNetworks/saturnv.git`

If you are using Git for your project and wish to use Saturn V as a submodule:

`git submodule add https://gitlab.com/AppliedNeuralNetworks/saturnv.git lib/saturnv`

### 4. Create the Executable Symbolic Link

Create a symbolic link from Saturn V's `main.py` to `bin/`:

`cd bin`
`ln -s ../lib/saturnv/main.py example`

This symlink can be created in any convenient location, such as `/usr/local/bin`; all that matters is its basename.

### 5. Run Your Application

When you execute `example`, Saturn V will determine its location and add your library path to `sys.path` if necessary. It will then import the package `example` and look for the `Application` class, either in the root of your namespace or in a sub-namespace named `application`. It will instantiate that class and call its `run()` method.

| If all you need is to import libraries and fire up your project, this is all you need. If you  want configuration loading and path information, read on. |
| --- |

### 6. Create Your Application's Configuration

Saturn V will search through a list of possible places to find configuration files named `$program_name.yaml`(here, `$program_name` is the basename of your executable, and `$app_dir` is your project's root directory):

 * If the project is self-contained, `$app_dir/conf`
 * The user configuration directory, `~/.config/$program_name`
 * In your personal project space, `~/project/$program_name/conf`
 * In the system-wide project location, `~/project/$program_name/conf`
 * In the system configuration in a named subdirectory, `/etc/$program_name`
 * In the system configuration, `/etc`

Each file found is loaded into one of a stack of configuration slots, so that when the value of an option is later requested, the highest-priority value is found first. Unless you specify a true value for `ignore_changes` when you instantiate `AppConf`, file-backed slots will have a special property: when a file is requested and the file has changed on disk, the configuration file for that slot will be reloaded from disk.

For the purposes of this example, create a file called `example.yaml` in the `conf` directory of your project root. An empty file is allowed, but you almost certainly want to begin adding your configuration here:

```yaml
foo: 'bar'
```

In your `Application` class, import `saturnv`, which is already in your `sys.path`. In your `__init__()`:

```python
self.conf = saturnv.AppConf()
```
`AppConf` is a dictionary-like object, returning the first (most-overriding) value for a given key, or if none is found, `KeyError`.

### 7. Going Further: Application Defaults and `argparse`

If your project needs to accept and handle command-line arguments, you can simply add the argparse namespace when initializing `AppConf`:

```python
defaults = {
    'mares': 'oats',
    'does': 'oats',
    'little_lambs': 'ivy',
    'kid': 'ivy',
}

self.conf = saturnv.AppConf(
    defaults=defaults,
    args=self.options.get_args(), # from argparse
)
```

Note that `defaults` and `args` can be used independently of each other -- you may have one or the other, both, or neither. These, too, are loaded into the configuration slots in `AppConf`.

### 8. Ignoring Runtime Changes in File Configurations

If you specify a truthy value for `ignore_changes` when you instantiate `AppConf`, the file-backed configurations won't auto-reload when a value is requested and the file has changed on disk. The default is to auto-reload configurations when the file on disk is changed.

### 9. Loading Other App Data

#### Finding and Loading Data Files

`conf.load_first_found` uses `conf.find()` to search for `filename` in a list of paths. You may specify your own list or use `saturnv.typical_data_paths` to search a provided list which includes typical paths. This list may include template variables which will be resolved by `interp_values`, which must be a dictionary-like object. (It is recommended that you pass your `AppConf` for `interp_values` as shown here.

The value returned is an instance of conf.ConfFileYAML unless you specify a `conf_type` such as `conf.ConfJSON`.

```python
self.data = saturnv.conf.load_first_found(
	'data.yaml', saturnv.typical_data_paths, self.conf)
```

#### Loading Data Files Explicitely

You may also use `conf`'s internal API to load data files. This is good strategy if you already know the location and type of your data file, or can easily derive them.

```python
self.my_data = saturnv.conf.ConfFileYAML('/path/to/file.yaml')
```

## The Future of Saturn V

### Guiding Philosophy

Saturn V tries to strike a balance between flexibility and brevity. The philosphy is simple: if your use case is a simple one, your code should not also be required to pay the cost of a more complex case. Of course, this is a desirable trait in every API, but is paramount in a system whose mission is to encourage you to make projects and explore your ideas and solve problems.

When in doubt, the guiding philosophy will be: _reduce the scope_ and nail it 100%. Your program should never be larger than the promises you can keep. In practice, this process can be painful, but examine what it may afford and you will see that the benefits far outweigh the trouble. Every line of code you write further "defines" your program -- that is, it decides more and more of what your program will _never_ do. A more productive approach is to expand the system by implication, tapping merrily on the delete key to remove obstacles to a better, cleaner design. This is not to say that a system should never be enlarged; it is to suppose that there may be another type of largeness which must not be forgotten.

### Early API Stability

Full speed ahead, meanwhile, to stabilize to the foward-facing API, prioritized by just how forward-facing a feature is. Your code shouldn't have to pay for deep implementation changes, and likewise, the API must not be locked into preserving ideas which couldn't ultimately stand on their own.

### The Way Forward: Patience, Courage, and Steady Progress

Saturn V is powerful, small and light. Is it well to preserve these; they are the best of features. This requires the patience to continually re-examine the intentions, and the courage to decline a careless adoption of features outside its simple scope. If a more comprehensive runtime-app framework is needed, they already exist or can be written separately. The whole idea behind Saturn V is to encourage new project creation to test ideas; let's work together to find the sweet spot.

