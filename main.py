#!/usr/bin/env python3

"""SaturnV Application Launcher:
   Symlink this file to your program's name in your bin subdirectory.
   For example, if your program is called "foo":
       ln -s /some/path/to/saturnv/main.py bin/foo
   Be sure your lib subdirectory contains a package called "foo"
   which provides foo.Application.run() or foo.application.Application.run()
"""

import sys
from os.path import dirname, join, realpath, islink

if not islink(sys.argv[0]):
    print(f'Error: {realpath(sys.argv[0])} '
          'should be a symlink to saturnv/main.py')
    sys.exit(1)

sys.path.insert(0, join(dirname(realpath(__file__)), 'lib'))
__import__('saturnv').launch()
