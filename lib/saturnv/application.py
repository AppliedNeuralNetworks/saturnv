import saturnv


class Application:
    default_conf = {
        "log_level": "warning",
        "verbose": False,
    }

    def __init__(self):
        self.log = saturnv.Logger()
        self.log.set_level(self.default_conf["log_level"])  # from defaults
        self.log.stderr_on()

        # self.options = Options(self)          # For example:
        # self.args = self.options.get_args()   # returns parser.parse_args()
        self.conf = saturnv.AppConf(
            defaults=Application.default_conf,
            # args=self.args,
        )

        self.log.set_level(self.conf["log_level"])  # from final config

    def run(self):
        print("Your saturnv application would run here.")
