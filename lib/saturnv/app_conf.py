import sys
import argparse
from copy import deepcopy
from string import Template
from operator import itemgetter
from os import getcwd, readlink
from os.path import (
    abspath,
    basename,
    dirname,
    exists,
    isdir,
    islink,
    join,
    expanduser,
)

from .conf import ConfFileYAML


class AppConf:
    levels = (
        # Level_Name_________File_________________________________
        ("overrides", None),  # Most overriding
        ("args", None),  # from argparse
        ("runtime", None),  # Paths and other computed info
        ("self-contained", "$app_dir/conf"),
        ("user", "~/.config/$app_name"),
        ("project_personal", "~/project/$app_name/conf"),
        ("project_system", "/srv/project/$app_name/conf"),
        ("etc_subdir", "/etc/$app_name"),  # Debian-like
        ("etc", "/etc"),  # Redhat-like
        ("defaults", None),  # Base defaults (esp. to satisfy args)
    )

    def __init__(self, defaults=None, args=None, ignore_changes=False):
        self.levels = deepcopy(AppConf.levels)
        self.stack = []

        for _ in self.levels:
            self.stack.append({})

        self.insert_defaults(defaults)
        self.insert_args(args)
        self.insert_path_info()
        self.insert_file_confs(ignore_changes)

    def insert_defaults(self, defaults):
        if defaults is not None:
            self.set_level("defaults", defaults)

    def insert_args(self, args):
        if args is None:
            return

        if isinstance(args, argparse.Namespace):
            conf_items = vars(args).items()
            conf = self.get_level("args")
            for key, value in conf_items:
                if value is not None:
                    conf[key] = value
        else:
            self.set_level("args", args)

    # TODO: any()/all()

    def override(self, key, value):
        overrides = self.get_level("overrides")
        overrides[key] = value

    def insert_path_info(self):
        self["app_name"] = basename(sys.argv[0])
        self["cwd"] = getcwd()

        if try_dir := find_app_dir():
            self["app_dir"] = try_dir
            self["lib_dir"] = join(try_dir, "lib")
        else:
            # Remove self-contained app directory path
            self.levels = self.levels[0:3] + self.levels[4:-1]

        self["data_paths"] = [
            f'/usr/local/lib/{self["app_name"]}/data',  # Local install
            f'/usr/local/share/{self["app_name"]}/data',
            f'/opt/{self["app_name"]}/data',
            f'/usr/lib/{self["app_name"]}/data',  # System install
            f'/usr/share/{self["app_name"]}/data',
        ]
        if "app_dir" in self:
            self["data_paths"].insert(0, join(self["app_dir"], "data"))

    def find_data_file(self, filename):
        for data_path in self["data_paths"]:
            maybe_data_file = join(data_path, filename)
            if exists(maybe_data_file):
                return maybe_data_file
        raise FileNotFoundError(
            f"Unable to locate data file {maybe_data_file}"
        )

    def find_every_data_file(self, filename):
        data_files = []
        for data_path in self["data_paths"]:
            maybe_data_file = join(data_path, filename)
            if exists(maybe_data_file):
                data_files.append(maybe_data_file)
        if not data_files:
            raise FileNotFoundError(
                "Unable to locate any data files named" f"{maybe_data_file}"
            )
        return data_files

    def insert_file_confs(self, ignore_changes=False):
        for level, _ in enumerate(self.levels):
            if not (_dir := self.levels[level][1]):
                continue
            _dir = expanduser(Template(_dir).substitute(self))
            _base = Template("$app_name.yaml").substitute(self)
            filename = join(_dir, _base)
            if exists(filename):
                self.stack[level] = ConfFileYAML(filename, ignore_changes)

    def merge_dicts(self, key):
        merged = {}
        for level in reversed(self.stack):
            merged.update(level.get(key, {}))
        return merged

    # Implement dict-like behavior
    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return default

    def keys(self):
        all_keys = []
        for level in self.stack:
            all_keys.extend(level.keys())
        return tuple(set(all_keys))

    def items(self):
        return dict((k, self.get(k)) for k in self.keys())

    def __str__(self):
        return str(self.items())

    def __repr__(self):
        return repr(self.items())

    def __getitem__(self, key):
        for conf in self.stack:
            if key in conf:
                return conf[key]
        raise KeyError(f"{key} not in configuration stack")

    def __setitem__(self, key, value):
        self.stack[self.level_pos("runtime")][key] = value

    def __contains__(self, key):
        return any(key in conf for conf in self.stack)

    # Stack manipulators
    def get_level(self, name):
        return self.stack[self.level_pos(name)]

    def set_level(self, name, conf):
        self.stack[self.level_pos(name)] = conf

    def level_pos(self, name):
        for level, _ in enumerate(self.levels):
            if self.levels[level][0] == name:
                return level
        return LookupError('Level "{name}" not in configuration stack')

    # Display configuration
    def show(self):
        for level, _ in enumerate(self.levels):
            conf = self.stack[level]
            print(f"    {self.levels[level][0]}:")
            if not conf:
                print("        -")
                continue
            for key, value in sorted(conf.items(), key=itemgetter(0)):
                print(f"        {key}: {value}")


def path_parent(path, levels=1):
    for _ in range(levels):
        path = dirname(path)
    return path


def resolve_rel_symlink(path, link):
    if link.startswith("/"):
        return link
    if not isdir(path):
        path = dirname(path)
    return abspath(join(path, link))


def find_app_dir():
    """Find the directory the application is nested in, in the
    case where the app is run from a checkout or unpacked tar.
    The app itself will be linked directly to this file,
    but additional symlinks may be present for convenience.
    For example, /usr/local/bin/fadelisk -> /opt/fadelisk/bin/fadelisk ->
    /opt/fadelisk/lib/python/saturnv/main.py. Start at sys.argv[0]
    and walk back through symlink chain.
    """
    walk_path = resolve_rel_symlink(".", sys.argv[0])
    while exists(walk_path) and islink(walk_path):
        next_link = resolve_rel_symlink(walk_path, readlink(walk_path))
        if not islink(next_link):
            return dirname(dirname(walk_path))
        walk_path = next_link
    return None
