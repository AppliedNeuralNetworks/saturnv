from . import conf
from .app_conf import AppConf
from .application import Application
from .daemon import Daemon
from .launcher import launch
from .lockfile import (
    Lockfile,
    LockfileEstablishError,
    LockfileFormatError,
    LockfileKillError,
    LockfileKillTimeoutError,
    LockfileLockedError,
    LockfileProcessNotRunningError,
)
from .logger import Logger, LoggerError

typical_data_paths = (
    "$app_dir/data",
    "~/.config/$program_name/data",
    "/var/lib/$program_name/data",
    "/usr/local/share/$program_name/data",
    "/usr/local/lib/$program_name/data",
    "/usr/share/$program_name/data",
    "/usr/lib/$program_name/data",
    "~/project/$program_name/data",
    "/srv/project/$program_name/data",
)
