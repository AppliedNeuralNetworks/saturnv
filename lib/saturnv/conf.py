import os
import json
import threading
from string import Template
from os.path import exists, join, expanduser
import yaml

try:
    from yaml import CLoader as YAMLLoader
except ImportError:
    from yaml import Loader as YAMLLoader


# pylint: disable=multiple-statements, missing-class-docstring
class ConfFileNotFoundError(OSError):
    pass


class ConfFormatError(Exception):
    pass


class ConfUpdateError(Exception):
    pass


# pylint: enable=multiple-statements, missing-class-docstring


class ConfDict(dict):
    """Simple configuration dictionary with thread safety"""

    def __init__(self, *args, **kwargs):
        """Initialize dict and prepare local lock instance."""
        super().__init__(*args, **kwargs)
        self.lock = threading.Lock()

    def soft_update(self, data):
        """Insert key-value pairs if the key is not present."""
        for key, value in list(data.items()):
            self.setdefault(key, value)

    def replace(self, data):
        """Replace self's contents ina thread-safe manner."""
        with self.lock:
            self.clear()
            self.update(data)


class ConfDictReadOnly(ConfDict):
    """Protected dictionary for file-backed configurations"""

    ro_error_exc = ConfUpdateError("Cannot alter read-only configuration")
    """Exception returned by functions that would normally modify data"""

    def __init__(self, ignore_changes=False):
        super().__init__()
        self.ignore_changes = ignore_changes

    # Disable updates to data
    # pylint: disable=multiple-statements
    def __setitem__(self, *args, **kwargs):
        raise self.ro_error_exc

    def __delitem__(self, *args, **kwargs):
        raise self.ro_error_exc

    def setdefault(self, *args, **kwargs):
        raise self.ro_error_exc

    def soft_update(self, *args, **kwargs):
        raise self.ro_error_exc

    def replace(self, *args, **kwargs):
        raise self.ro_error_exc

    def pop(self, *args, **kwargs):
        raise self.ro_error_exc

    def popitem(self, *args, **kwargs):
        raise self.ro_error_exc

    # pylint: enable=multiple-statements


class ConfFileYAML(ConfDictReadOnly):
    """Configuration from YAML file (with optional auto-updates)"""

    def __init__(self, filename, ignore_changes=False):
        super().__init__(ignore_changes=ignore_changes)
        self.filename = filename
        self.timestamp = None
        self.refresh()

    def __getitem__(self, key):
        if not self.ignore_changes:
            self.refresh()
        return ConfDictReadOnly.__getitem__(self, key)

    def get(self, key, default=None):
        if not self.ignore_changes:
            self.refresh()
        return ConfDictReadOnly.get(self, key, default)

    def refresh(self):
        if (mtime := os.stat(self.filename).st_mtime) != self.timestamp:
            try:  # pylint: disable=too-many-try-statements
                with open(self.filename, encoding="utf-8") as yaml_file:
                    data = yaml.load(yaml_file, Loader=YAMLLoader) or {}
            except:  # pylint: disable=bare-except
                return
            if not isinstance(data, dict):
                raise ConfFormatError("ConfFileYAML must be a dictionary")
            ConfDict.replace(self, data)
            self.timestamp = mtime


class ConfFileJSON(ConfDictReadOnly):
    """Configuration from JSON file (with optional auto-updates)"""

    def __init__(self, filename, ignore_changes=False):
        super().__init__(ignore_changes=ignore_changes)
        self.filename = filename

        self.timestamp = None
        self.refresh()

    def __getitem__(self, key):
        if not self.ignore_changes:
            self.refresh()
        return ConfDictReadOnly.__getitem__(self, key)

    def refresh(self):
        if (mtime := os.stat(self.filename).st_mtime) != self.timestamp:
            with open(self.filename, encoding="utf-8") as json_file:
                ConfDict.replace(self, json.load(json_file))
            self.timestamp = mtime


class ConfFileList(list):
    """Line-wise data from text file (with optional auto-updates)"""

    def __init__(self, filename, ignore_changes=False):
        super().__init__()
        self.filename = filename
        self.ignore_changes = ignore_changes

        self.timestamp = None
        self.lock = threading.Lock()
        self.refresh()

    def __iter__(self):
        if not self.ignore_changes:
            self.refresh()
        return list.__iter__(self)

    def __getitem__(self, key):
        if not self.ignore_changes:
            self.refresh()
        return list.__getitem__(self, key)

    def __getslice__(self, i=None, j=None):
        if not self.ignore_changes:
            self.refresh()
        return self[i:j]

    def refresh(self):
        if (mtime := os.stat(self.filename).st_mtime) != self.timestamp:
            with self.lock:
                with open(self.filename, encoding="utf-8") as file:
                    self[:] = file.readlines()
                self.timestamp = mtime


class ConfFileContents:
    """String data from entire text file (with optional auto-updates)"""

    def __init__(self, filename, ignore_changes=False):
        self.filename = filename
        self.ignore_changes = ignore_changes

        self.timestamp = None
        self.data = None
        self.lock = threading.Lock()
        self.refresh()

    def refresh(self):
        if (mtime := os.stat(self.filename).st_mtime) != self.timestamp:
            with self.lock:
                with open(self.filename, encoding="utf-8") as file:
                    self.data = file.read()
                self.timestamp = mtime

    def contents(self):
        if not self.ignore_changes:
            self.refresh()
        return self.data

    def __str__(self):
        return str(self.data)


def find(filename, places, interp_values=None, first_only=False):
    if interp_values is None:
        interp_values = {}
    found = []
    for place in places:
        path = expanduser(Template(place).substitute(interp_values))
        _filename = join(path, filename)
        if exists(_filename):
            if first_only:
                return _filename
            found.append(_filename)
    if found:
        return found
    raise ConfFileNotFoundError(f"Conf file not found: {filename}")


def load_first_found(
    filename,
    places,
    interp_values=None,
    conf_type=ConfFileYAML,
    ignore_changes=False,
):
    if interp_values is None:
        interp_values = {}
    found = find(filename, places, interp_values, first_only=True)
    return conf_type(found, ignore_changes)
