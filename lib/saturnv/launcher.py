"""Launcher mechanics:
   These functions are used by the symlinked launcher script to load
   and run the target application.
"""

import sys
from os.path import basename, exists, join
from .app_conf import find_app_dir


def launch():
    """Load app and run it."""
    # If app is in self contained directory, such as a checkout
    # or unpacked tar file, add library subdirectories sys.path
    if app_dir := find_app_dir():
        app_lib_dir = join(app_dir, "lib")
        prepend_sys_path(app_lib_dir)
        prepend_sys_path(join(app_lib_dir, "python"))
    # Import eponymous package
    app_name = basename(sys.argv[0]).replace("-", "_")
    __import__(app_name)
    # Run the app.
    find_application_class(app_name).run()


def prepend_sys_path(path):
    """Insert path at head of sys.path"""
    if exists(path) and path not in sys.path:
        sys.path.insert(0, path)


def find_application_class(app_name):
    """Locate the Application class inside the app's eponymous package."""
    app = sys.modules[app_name]
    if hasattr(app, "Application"):  # at top (lifted by __init__.py)
        return app.Application()
    if hasattr(app, "application") and hasattr(  # within package submodule
        app.application, "Application"
    ):
        return app.application.Application()

    print(
        "Need Application() or application.Application() in "
        + f"package {app_name}"
    )
    sys.exit(1)
